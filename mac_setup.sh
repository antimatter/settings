#! /bin/sh

# From https://www.topbug.net/blog/2013/04/14/install-and-use-gnu-command-line-tools-in-mac-os-x/

if [ "$(uname -s)" != "Darwin" ];
    echo "This is for Mac OS only"
    exit -1
fi

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

export PATH="$(brew --prefix coreutils)/libexec/gnubin:/usr/local/bin:$PATH"
brew install coreutils
brew install gnu-tar

# alternatively
brew install binutils
brew install diffutils
brew install ed --default-names
brew install findutils
brew install gawk
brew install gnu-indent
brew install gnu-sed
brew install gnu-which
brew install gnutls
brew install gnutls
brew install gpg
brew install grep
brew install grep
brew install gzip
brew install nmap
brew install screen
brew install tmux
brew install tmuxinator
brew install watch
brew install wdiff --with-gettext
brew install wget

brew install jq
brew install pyenv
brew install openssl readline sqlite3 xz zlib
pyenv install 2.7.16
pyenv install 3.7.4
pyenv global 3.7.4

brew install git-crypt
brew install gpg

brew install pyenv-virtualenv
#git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv


# https://gist.github.com/jvenator/9672772a631c117da151
brew install qpdf

mkdir ~/bin
cat > ~/bin/brew <<EOF
#!/bin/sh
# check if pyenv is available
# edit: fixed redirect issue in earlier version
if which pyenv >/dev/null 2>&1; then
  # assumes default location of brew in `/usr/local/bin/brew`
  /usr/bin/env PATH="${PATH//$(pyenv root)\/shims:/}" /usr/local/bin/brew "$@"
else
  /usr/local/bin/brew "$@"
fi
EOF
