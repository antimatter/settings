#! /bin/bash

dir=$(readlink -f $(dirname $0))

echo $dir

function install_config () {
    backup="$1"
    location="$HOME/$2"
    echo "location = $location"
    if [ ! -e "$location" ]; then
        echo "$(basename $backup) -> $location .."
        ln -rs "$backup" "$location";
    else
        echo "$location already exists"
    fi
}

install_config bashrc .bashrc
install_config bash_profile .bash_profile
install_config tmuxrc .tmux.conf
install_config vimrc .vimrc
install_config ssh_config .ssh/config
install_config pystartup .pystartup

# Install git hubflow
if git hf >/dev/null 2>&1; then
    echo "git hubflow is already installed"
else
    echo "Installing git hubflow .."
    pwd=$(pwd)
    git clone https://github.com/datasift/gitflow /tmp/gitflow
    cd /tmp/gitflow
    sudo ./install.sh
    cd "$pwd"
fi


