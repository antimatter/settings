#!/usr/bin/perl -w

use strict;
use warnings;
use diagnostics;
use Data::Dumper qw( Dumper );
use File::Basename qw( basename );
use Getopt::Long qw( GetOptions );

## Global variables
my (%options, %config);
our ($_ME, $_AUTHOR, $_VERSION, $_PACKAGE, $_COPYRIGHT_YEARS, $_BUG_ADDRESS);
$_ME = basename $0;
$_VERSION = "0.0";
$_PACKAGE = "Misc";
$_COPYRIGHT_YEARS = "2013";
$_AUTHOR = "Gururaj K";
$_BUG_ADDRESS = "gururaj.krishnamurthy\@gmail.com";

## Default options
%options = (
            'verbosity' => 2,
            'logfile' => "/var/qa/$_ME.log",
           );

$SIG{__WARN__} = sub { my_warn (@_) };

init() and quit(main());


## ----------------------------- Subroutines begin -----------------------------

sub help {
    print <<"__HELP__";
Usage: $_ME [OPTION]... [FILE]...
<DESCRIPTION>

Mandatory arguments to long options are mandatory for short options too.

  -l, --logfile=LOGFILE     use LOGFILE for logging.
                            [Default: $options{logfile}]
  -s, --silent              supress normal output
  -q, --quiet               supress all output
  -v, --verbose             increase verbosity level. Use more v's for more
                            verbose output

      --help        display this help and exit
      --version     output version information and exit

<CAVEATS>

<RETURN VALUES>

Report bugs to <$_BUG_ADDRESS>.
__HELP__
    quit(0);
}

sub parse_args {
    Getopt::Long::Configure qw( gnu_getopt no_ignore_case );
    GetOptions (\%options,
                "logfile|l",
                "quiet|q",
                "verbose|v" => sub { ++$options{'verbosity'} },
                "help|h|?",
                "version|V",
               ) or usage();
    $options{'verbosity'} = 1 if defined $options{silent};
    $options{'verbosity'} = 0 if defined $options{quiet};

    help() and return 0 if $options{help};
    version() and return 0 if $options{version};

    if (scalar @ARGV == 0) {
        error("Arguments expected");
        usage();
    }

    return 1;
}

sub quit {
    exit shift;
}

sub my_warn {
    print STDERR "$_ME: @_\n";
}

sub error {
    print STDERR "$_ME: @_\n";
    quit 1;
}

sub version {
    print <<__VERSION__;
$_ME ($_PACKAGE) $_VERSION
Written by $_AUTHOR.

Copyright (C) $_COPYRIGHT_YEARS $_AUTHOR.
__VERSION__
    quit(0);
}

sub usage {
    print STDERR <<"__USAGE__";
Usage: $_ME [options]
Try `$_ME --help' for more information.
__USAGE__
    quit(1);
}


sub init {
    parse_args() or return 0;
    return 1;
}

sub main {
    foreach my $input (@ARGV) {
        print "Processing <$input> ...\n";
        
    }

    return 1;
}


