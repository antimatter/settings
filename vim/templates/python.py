#! /usr/bin/env python
## 
## Started: 
## Last Modified: 
## 
##################################################################
## Real Programmers never document ... If it was hard to write, ##
##  it should be hard to understand                             ##
##################################################################

import sys, os
from ConfigParser import *
from optparse import OptionParser
from tempfile import mkdtemp
from shutil import rmtree
from pprint import pprint
## import tarfile

_PRG = os.path.basename (sys.argv[0])
TMPDIR = mkdtemp ()
VERBOSITY = 0

def parse_cmd_line (usage, args):
    """ Parse command line and return the dictionary """

    parser = OptionParser(usage)

    if len (args) == 0:
        error ("insufficient arguments\n"
               "Try `%s --help' for more information." % _PRG)
        quit (2)

##     parser.add_option ('-l', '--list', action='store_true',
##                        dest='list',
##                        help='List')
    parser.add_option ("-v", '--verbosity', action="count",
                      dest="verbosity", default=0,
                      help='Verbosity. Use more v\'s to increase verbosity')

    return parser.parse_args(args)

def v1 (message):
    if getverb () >= 1:
        print ("%s" % message)

    return

def v2 (message):
    if getverb () >= 2:
        print ("%s" % message)

    return

def v3 (message):
    if getverb () >= 3:
        print ("%s" % message)

    return

def quit (return_value):
    v1 ("Cleaning temporary directory `%s'" % TMPDIR)
    rmtree (TMPDIR)
    sys.exit (return_value)

    return

def error (message):
    sys.stderr.write ("%s: %s\n" % (_PRG, message))
    return

def getverb ():
    return VERBOSITY

def setverb (val):
    global VERBOSITY

    VERBOSITY = val
    return

def main ():
    usage = "Usage: %prog [options]"
    (options, args) = parse_cmd_line (usage, sys.argv[1:])
    setverb (options.verbosity)

    return 0

quit (main ())

