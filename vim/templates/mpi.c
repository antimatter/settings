/*
 * Name:
 *
 * Description:
 *
 * Author: Gururaj K (gururaj.krishnamurthy@gmail.com)
 *
 * Started: Wed 07 Aug 2013 11:08:11 PM IST
 * Last Modified: 
 *
*/


#include    <stdio.h>
#include    <mpi.h>

#define     TAGNO       1

int
main (int argc, char **argv)
{
    int rank, size;
    MPI_Status status;
//    char hostname[256];

    /* Get Host Name */
//    gethostname (hostname, 256);

    /* Initialize MPI */
    if (MPI_Init (&argc, &argv) != MPI_SUCCESS) {
        fprintf (stderr, "%s: MPI initialization error\n");
        return EXIT_FAILURE;
    }

    /* Number of processes */
    MPI_Comm_rank (MPI_COMM_WORLD, &rank);

    /* Get the rank for the process */
    MPI_Comm_size (MPI_COMM_WORLD, &size);

    /* Actual code should now begin */

    if (rank == 0) {
//        mastercode ();
    }
    else {
//        slavecode ();
    }

    /* Wrap up */
    MPI_Finalize ();

    return EXIT_SUCCESS;
}


/*
int
slavecode () {

    return 0;
}
*/

/*
int
mastercode () {

    return 0;
}
*/


